import networkx as nx
import argparse
import math
import matplotlib
from random import seed, randrange, choice
try:
    import matplotlib.pyplot as plt
except:
    raise

class JellyFish():
    "JellyFish Topology"

    def __init__(self, hosts = 16, sw = 20, ports = 4):
        self.hosts = hosts
        self.sw = sw
        self.portCount = ports
        seed()
        self.G = nx.Graph()

    def buildJellyFish(self):
        
        switches = []
        openPortCount = []
        hosts = []
        links = set()
        switchCount = self.sw
        limit = 0

        for i in range(1, self.hosts + 1):
            host = "h_" + str(i)
            hosts.append(host)

        for i in range(1, self.sw + 1):
            switch = "s_" + str(i)
            switches.append(switch)
            openPortCount.append(self.portCount)

        for i in range(self.hosts):
            self.G.add_edge(hosts[i], switches[i % self.sw])
            openPortCount[i % self.sw] = openPortCount[i % self.sw] - 1

        while switchCount > 1 and limit < 10:
            s1 = randrange(self.sw)
            while openPortCount[s1] == 0:
                s1 = randrange(self.sw)
            s2 = randrange(self.sw)
            while openPortCount[s2] == 0:
                s2 = randrange(self.sw)

            if (s1,s2) in links:
                limit = limit + 1
            else:
                limit = 0
                if openPortCount[s1] > 0 and openPortCount[s2] > 0:
                    links.add((s1,s2))
                    links.add((s2,s1))
                    self.G.add_edge(switches[s1], switches[s2])
                    self.G.add_edge(switches[s2], switches[s1])
                    openPortCount[s1] = openPortCount[s1] - 1
                    openPortCount[s2] = openPortCount[s2] - 1

                    if openPortCount[s1] == 0:
                        switchCount = switchCount - 1
                    if openPortCount[s2] == 0:
                        switchCount = switchCount - 1

        if switchCount > 0:
            for i in range(self.sw):
                while openPortCount[i] > 1:
                    while True:
                        link = choice(list(links))
                        if (i,link[0]) in links:
                            continue
                        if (i,link[1]) in links:
                            continue

                        links.remove(link)
                        links.remove(reversed(link))
                        self.G.remove_edge(switches[link[0]], switches[link[1]])
                        self.G.remove_edge(switches[link[1]], switches[link[0]])

                        links.add((link[0], i))
                        links.add((i, link[0]))
                        self.G.add_edge(switches[link[0]], i)
                        self.G.add_edge(i, switches[link[0]])

                        links.add((link[1], i))
                        links.add((i, link[1]))
                        self.G.add_edge(switches[link[1]], i)
                        self.G.add_edge(i, switches[link[1]])

                        openPortCount[i] = openPortCount[i] - 2
                        break

        self.switches = switches
        self.hosts = hosts

    def printGraph(self):
        print "graph should come here"
        #print self.G.adj
        print nx.is_connected(self.G)
        #nx.draw_networkx(self.G)
        nx.draw(self.G, pos=nx.shell_layout(self.G, [self.switches, self.hosts]), node_size=10, alpha=0.5)
        #print "Diameter: " + str(nx.diameter(self.G))
        #print "Average shortest path length: "  + str(nx.average_shortest_path_length(self.G))
        #print "Density: " + str(nx.density(self.G))
        hist = nx.degree_histogram(self.G)
        histogram_dict = {}
        for i in range(0, len(hist)):
            if hist[i] != 0:
                histogram_dict[i] = hist[i]
        print "hist: " + str(hist)
        print "Degree Histogram: " + str(histogram_dict)
        plt.show()

def main():
    JF = JellyFish(128, 80, 16)
    print "God Help"
    JF.buildJellyFish()
    JF.printGraph()
    
if __name__ == '__main__':
    main()

