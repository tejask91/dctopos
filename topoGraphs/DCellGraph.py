import networkx as nx
import argparse
import math
import matplotlib
try:
    import matplotlib.pyplot as plt
except:
    raise
'''
parser = argparse.ArgumentParser(description="DCell")
parser.add_argument('--ns', '-n',
                    type=float,
                    help="Number of Servers",
                    required=True)


parser.add_argument('--levels', '-l',
                    type=float,
                    help="Number of DCell Levels",
                    required=True)

args = parser.parse_args()
'''



class DCell():
    "DCell Topology for l and n and k=3"

    def __init__(self,l,n):
        self.n = n
        self.l = l
        #self.g = [0 for i in range(self.l+1)]
        #self.t = [0 for i in range(self.l+1)]
        self.g = []
        self.t = []
        self.setTandG()
        #print self.g
        #print self.t
        self.G = nx.Graph()
        self.switches = 0
    
    def setTandG(self):
        self.g.append(1)
        self.t.append(self.n)
        for i in range(1,self.l+1):
            self.g.append(self.t[i-1] + 1)
            self.t.append(self.g[i]*self.t[i-1])

    def BuildDCell1(self):
        self.BuildDCell("",self.l,self.n,0)

    def printGraph(self):
        #print self.G.adj
        print nx.nodes(self.G)
        print len(nx.nodes(self.G))
        #nx.draw_networkx(self.G)
        #plt.show()

    def BuildDCell(self,prefix,l1,n, switchIndex):
        if(l1==0):
            for i in range(0,n):
                self.G.add_edge(prefix+str(i),"Switch"+str(switchIndex))
                #print "Adding Edge to Switch: "+str(switchIndex)
            #switchIndex = switchIndex + 1
            return

        #print "L:"+str(l1)
        for i in range(0,self.g[l1]):
            #print "Calling for l value:"+prefix+str(i)+" "+str(l1-1)+ " "+str(self.n)
            self.BuildDCell(prefix+str(i), l1-1, self.n, switchIndex)
            switchIndex += 1
            self.switches = switchIndex

        for i in range(0,self.t[l1-1]):
            for j in range(i+1,self.g[l1]):
                uid1 = j - 1
                uid2 = i
                n1 = prefix+str(i)+str(uid1)
                n2 = prefix+str(j)+str(uid2)
                self.G.add_edge(n1,n2)

        return


def main():
     D = DCell(1,319)
     #print "God Help"
     D.BuildDCell1()
     D.printGraph()
    
if __name__ == '__main__':
    main()

