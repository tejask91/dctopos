import networkx as nx
import argparse
import math
import matplotlib   
try:
    import matplotlib.pyplot as plt
except:
    raise

class FatTree():
    "2-level FatTree topology for number of pods"

    def __init__(self,pods):
        self.pods = pods
        self.G = nx.Graph()

    def buildFat(self):
        k = self.pods
        for i in range(0, k):
            #Hosts
            for j in range(0,int(math.pow(k/2, 2))):
                #print "h_"+str(i)+str(j) + " es_"+str(i)+str(j/2)
                self.G.add_edge("h_"+str(i)+"_"+str(j),"es_"+str(i)+"_"+str((2*j/k)))

            #Edge and aggregate switches
            for j in range(0, int(k/2)):
                for l in range(0, int(k/2)):
                    #print "es_"+str(i)+str(l) + " as_"+str(i)+str(j)
                    self.G.add_edge("es_"+str(i)+"_"+str(l),"as_"+str(i)+"_"+str(j))

            #core switches
            for j in range(0, int(k/2)):
                for l in range(int((k*j)/2), int((k*(j+1))/2)):
                    #print "as_"+str(i)+str(j) + " cs_"+str(l)
                    self.G.add_edge("as_"+str(i)+"_"+str(j), "cs_"+str(l))


    def is_tree(self, G):
        if(nx.number_of_nodes(G) != nx.number_of_edges(G) + 1):
            return False
        return nx.is_connected(G)

    def printGraph(self):
        #print nx.betweenness_centrality(self.G)
        #print nx.number_connected_components(self.G)
        #print "Number of hosts:"+str(self.count)
        #print "The diameter of the Graph:"+str(nx.diameter(self.G))
        '''pathLen = set()
        for path in sorted(nx.all_simple_paths(self.G,"h01","h32")):
            pathLen.add(len(path))
        for s in pathLen:
            print s'''
        #count = 0
        #result = set(nx.shortest_path(self.G,"h01","h12"))
        #for path in nx.all_simple_paths(self.G,"h01","h12"):
        #    print path
        #    result.intersection_update(path)
        #    count += 1
        #print "Number of Paths:"+str(count)
        #print "Nodes in all Paths:"+str(result)
        #print self.G.adj
        #print nx.degree_histogram(self.G)
        #print nx.density(self.G)
        #nx.draw_networkx(self.G)
        #for x in nx.connected_components(self.G):
        #    print x
        #print "Diameter: " + str(nx.diameter(self.G))
        #print "Average shortest path length: "  + str(nx.average_shortest_path_length(self.G))
        print "Density: " + str(nx.density(self.G))
        hist = nx.degree_histogram(self.G)
        histogram_dict = {}
        for i in range(0, len(hist)):
            if hist[i] != 0:
                histogram_dict[i] = hist[i]
        print "hist: " + str(hist)
        print "Degree Histogram: " + str(histogram_dict)
        #plt.show()


def main():
    F = FatTree(32)
    print "God Help"
    F.buildFat()
    F.printGraph()
    
if __name__ == '__main__':
    main()

