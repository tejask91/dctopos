"Library of some Data Center Topologies"

from mininet.topo import Topo
from mininet.net import Mininet
import math
from random import seed, randrange, choice

class JellyFish(Topo):
    "JellyFish topology for jellyHosts, jellySwitches and ports"

    def __init__(self):
        
        self.jellyHosts = 16
        self.sw = 20
        self.portCount = 4
        seed()
        self.NodeDict = {}


        jellySwitches = []
        openPortCount = []
        jellyHosts = []
        jellyLinks = set()
        switchCount = self.sw
        limit = 0
        NodeDict = {}
        foraddLink = []


        # Initialize topology
        Topo.__init__( self )



        for i in range(1, self.jellyHosts + 1):
            host = "h_" + str(i)
            NodeDict[host] = self.addHost(host)
            jellyHosts.append(host)

        for i in range(1, self.sw + 1):
            switch = "s_" + str(i)
            NodeDict[switch] = self.addSwitch(switch)
            jellySwitches.append(switch)
            openPortCount.append(self.portCount)

        for i in range(self.jellyHosts):
            #self.addLink(jellyHosts[i], jellySwitches[i % self.sw])
            foraddLink.append((jellyHosts[i], jellySwitches[i % self.sw]))
            openPortCount[i % self.sw] = openPortCount[i % self.sw] - 1

        while switchCount > 1 and limit < 10:
            s1 = randrange(self.sw)
            while openPortCount[s1] == 0:
                s1 = randrange(self.sw)
            s2 = randrange(self.sw)
            while openPortCount[s2] == 0:
                s2 = randrange(self.sw)

            if (s1,s2) in jellyLinks:
                limit = limit + 1
            else:
                limit = 0
                if openPortCount[s1] > 0 and openPortCount[s2] > 0:
                    jellyLinks.add((s1,s2))
                    jellyLinks.add((s2,s1))
                    #self.addLink(jellySwitches[s1], jellySwitches[s2])
                    #self.addLink(jellySwitches[s2], jellySwitches[s1])
                    foraddLink.append((jellySwitches[s2], jellySwitches[s1]))
                    foraddLink.append((jellySwitches[s2], jellySwitches[s1]))
                    openPortCount[s1] = openPortCount[s1] - 1
                    openPortCount[s2] = openPortCount[s2] - 1

                    if openPortCount[s1] == 0:
                        switchCount = switchCount - 1
                    if openPortCount[s2] == 0:
                        switchCount = switchCount - 1

        if switchCount > 0:
            for i in range(self.sw):
                while openPortCount[i] > 1:
                    while True:
                        jellyLink = choice(list(jellyLinks))
                        if (i,jellyLink[0]) in jellyLinks:
                            continue
                        if (i,jellyLink[1]) in jellyLinks:
                            continue

                        jellyLinks.remove(jellyLink)
                        jellyLinks.remove(reversed(jellyLink))
                        #self.G.remove_edge(jellySwitches[jellyLink[0]], jellySwitches[jellyLink[1]])
                        #self.G.remove_edge(jellySwitches[jellyLink[1]], jellySwitches[jellyLink[0]])
                        foraddLink.remove((jellySwitches[jellyLink[0]], jellySwitches[jellyLink[1]]))
                        foraddLink.remove((jellySwitches[jellyLink[1]], jellySwitches[jellyLink[0]]))

                        jellyLinks.add((jellyLink[0], i))
                        jellyLinks.add((i, jellyLink[0]))
                        #self.addLink(jellySwitches[jellyLink[0]], i)
                        #self.addLink(i, jellySwitches[jellyLink[0]])
                        foraddLink.append((jellySwitches[jellyLink[0]], i))
                        foraddLink.append((i, jellySwitches[jellyLink[0]]))
                     
                        jellyLinks.add((jellyLink[1], i))
                        jellyLinks.add((i, jellyLink[1]))
                        #self.addLink(jellySwitches[jellyLink[1]], i)
                        #self.addLink(i, jellySwitches[jellyLink[1]])
                        foraddLink.append((jellySwitches[jellyLink[1]], i))
                        foraddLink.append((i, jellySwitches[jellyLink[1]]))

                        openPortCount[i] = openPortCount[i] - 2
                        break
    

        linksToAdd = []
        [linksToAdd.append(item) for item in foraddLink if item and reversed(item) not in linksToAdd]
        
        
        
        for (n1, n2) in linksToAdd:
            if (n1!=n2):
                self.addLink(NodeDict[n1],NodeDict[n2])


topos = { 'jelly':  ( lambda: JellyFish() ) }
 
