"Library of some Data Center Topologies"

from mininet.topo import Topo
from mininet.net import Mininet

class DCell(Topo):
    "DCell topology for l-levels and n-hosts per DCell node"

    def __init__(self):
        
        self.l = 1
        self.n = 4
        self.gVal = [0 for x in range(self.l+1)]
        self.t = [0 for x in range(self.l+1)]
        self.Nodedict = {} 
        # Initialize topology
        Topo.__init__( self )
        
        # Adding Hosts
        for i in range(self.n+1):
            for j in range(self.n+1):
                self.Nodedict[str(i)+str(j)] =  self.addHost(str(i)+str(j))

        # Adding Switches
        for i in range(self.n+1):
            self.Nodedict['Switch'+str(i)] = self.addSwitch('Switch'+str(i))
       
        # Prepare Levels List
        self.gVal[0]=1
        self.t[0]=self.n 
        for i in range(1,self.l+1):
            self.gVal[i] = self.t[i-1] + 1
            self.t[i] = self.gVal[i]*self.t[i-1]

        # Call the recursive algorithm
        self.buildCell('',self.l,self.n,0)

    def buildCell(self, prefix, level, numHosts, switchIndex):
    
        if(level==0):
            for i in range(0,numHosts):
                self.addLink(self.Nodedict[prefix+str(i)],self.Nodedict['Switch'+str(switchIndex)])
            return

        for i in range(0,self.gVal[level]):
            self.buildCell(prefix+str(i), level-1, numHosts, switchIndex)
            switchIndex += 1

        for i in range(0,self.t[level-1]):
            for j in range(i+1,self.gVal[level]):
                uid1 = j - 1
                uid2 = i
                n1 = prefix+str(i)+str(uid1)
                n2 = prefix+str(j)+str(uid2)
                self.addLink(self.Nodedict[n1],self.Nodedict[n2])
        return


topos = { 'dcell':  ( lambda: DCell() ) }
 
