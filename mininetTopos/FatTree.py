"Library of some Data Center Topologies"

from mininet.topo import Topo
from mininet.net import Mininet
import math

class FatTree(Topo):
    "FatTree topology for k-pods"

    def __init__(self):
        
        self.pods = 4
        kVal = self.pods
        self.NodeDict = {}


        # Initialize topology
        Topo.__init__( self )

        for i in range(0, kVal):
            #Hosts
            for j in range(0,int(math.pow(kVal/2, 2))):
                self.NodeDict["h_"+str(i)+"_"+str(j)] = self.addHost("h_"+str(i)+"_"+str(j))
                self.NodeDict["es_"+str(i)+"_"+str((2*j/kVal))] = self.addSwitch("es_"+str(i)+"_"+str((2*j/kVal)))
                self.addLink(self.NodeDict["h_"+str(i)+"_"+str(j)],self.NodeDict["es_"+str(i)+"_"+str((2*j/kVal))])

            #Edge and aggregate switches
            for j in range(0, int(kVal/2)):
                for l in range(0, int(kVal/2)):
                    self.NodeDict["es_"+str(i)+"_"+str(l)] = self.addSwitch("es_"+str(i)+"_"+str(l))
                    self.NodeDict["as_"+str(i)+"_"+str(j)] = self.addSwitch("as_"+str(i)+"_"+str(j))
                    self.addLink(self.NodeDict["es_"+str(i)+"_"+str(l)],self.NodeDict["as_"+str(i)+"_"+str(j)])

            #core switches
            for j in range(0, int(kVal/2)):
                for l in range(int((kVal*j)/2), int((kVal*(j+1))/2)):
                    self.NodeDict["as_"+str(i)+"_"+str(j)] = self.addSwitch("as_"+str(i)+"_"+str(j))
                    self.NodeDict["cs_"+str(l)] = self.addSwitch("cs_"+str(l))
                    self.addLink(self.NodeDict["as_"+str(i)+"_"+str(j)], self.NodeDict["cs_"+str(l)])



topos = { 'fattree':  ( lambda: FatTree() ) }
 
